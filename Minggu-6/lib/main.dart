import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final List<String> gambar = ["1.gif", "2.gif"];

  static const Map<String, Color> colors = {
    '1': Colors.cyan,
    '2': Colors.blueGrey,
  };
  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Minggu ke-6',
        home: Scaffold(
          appBar: AppBar(
            title: const Text('Anton Sebrianto'),
          ),
          body: Center(
            child: Container(
              decoration: BoxDecoration(
                gradient: SweepGradient(colors: [
                  Colors.deepOrange,
                  Colors.deepPurple,
                  Colors.blue
                ]),
              ),
              child: new PageView.builder(
                  controller: new PageController(viewportFraction: 0.8),
                  itemCount: gambar.length,
                  itemBuilder: (BuildContext context, int i) {
                    return new Padding(
                      padding: new EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 50.0),
                      child: new Material(
                        elevation: 8.0,
                        child:
                            new Stack(fit: StackFit.expand, children: <Widget>[
                          new Hero(
                            tag: gambar[i],
                            child: new Material(
                              child: new InkWell(
                                  child: new Flexible(
                                    flex: 1,
                                    child: Container(
                                      color: colors.values.elementAt(i),
                                      child: new Image.asset(
                                        "assets/images/${gambar[i]}",
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                  onTap: () => Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (BuildContext context) =>
                                              new HalamanKedua(
                                                gambar: gambar[i],
                                                colors:
                                                    colors.values.elementAt(i),
                                              )))),
                            ),
                          )
                        ]),
                      ),
                    );
                  }),
            ),
          ),
        ));
  }
}

class HalamanKedua extends StatefulWidget {
  HalamanKedua({required this.gambar, required this.colors});
  final String gambar;
  final Color colors;

  @override
  State<HalamanKedua> createState() => _HalamanKeduaState();
}

class _HalamanKeduaState extends State<HalamanKedua> {
  Color warna = Colors.grey;

  void _pilihannya(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            title: new Text("Halaman Kedua"),
            backgroundColor: Colors.limeAccent,
            actions: <Widget>[
              new PopupMenuButton<Pilihan>(
                onSelected: _pilihannya,
                itemBuilder: (BuildContext context) {
                  return listPilihan.map((Pilihan x) {
                    return new PopupMenuItem<Pilihan>(
                      child: new Text(x.teks),
                      value: x,
                    );
                  }).toList();
                },
              )
            ],
          ),
          body: new Stack(
            children: <Widget>[
              new Container(
                decoration: new BoxDecoration(
                    gradient: new RadialGradient(
                        center: Alignment.center,
                        colors: [
                      Colors.green,
                      Colors.yellow,
                      Colors.lightGreen
                    ])),
              ),
              new Center(
                child: new Hero(
                    tag: widget.gambar,
                    child: new ClipOval(
                        child: new SizedBox(
                            width: 200.0,
                            height: 200.0,
                            child: new Material(
                                child: new Material(
                              child: new InkWell(
                                onTap: () => Navigator.of(context).pop(),
                                child: new Flexible(
                                  flex: 1,
                                  child: Container(
                                    color: widget.colors,
                                    child: new Image.asset(
                                      "assets/images/${widget.gambar}",
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                            ))))),
              )
            ],
          ),
        ));
  }
}

class Pilihan {
  const Pilihan({required this.teks, required this.warna});
  final String teks;
  final Color warna;
}

List<Pilihan> listPilihan = const <Pilihan>[
  const Pilihan(teks: "Red", warna: Colors.red),
  const Pilihan(teks: "Green", warna: Colors.green),
  const Pilihan(teks: "Blue", warna: Colors.blue),
];
