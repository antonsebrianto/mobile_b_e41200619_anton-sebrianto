import 'package:flutter/material.dart';

class materialApp extends StatelessWidget {
  const materialApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.dashboard),
          title: Text('Belajar Material App Scaffold'),
          actions: <Widget>[
            Icon(Icons.search),
          ],
          actionsIconTheme: IconThemeData(color: Colors.redAccent),
          backgroundColor: Colors.blueGrey,
          bottom: PreferredSize(
            child: Container(
              color: Colors.orange,
              height: 4.0,
            ),
            preferredSize: Size.fromHeight(4.0),
          ),
          centerTitle: true,
        ),
        // Perubahan Baru
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.redAccent,
          child: Text('+'),
          onPressed: () {},
        ),
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.redAccent, shape: BoxShape.circle),
              ),
              Container(
                width: 50,
                height: 50,
                decoration: BoxDecoration(
                    color: Colors.blueAccent, shape: BoxShape.circle),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        color: Colors.cyanAccent, shape: BoxShape.circle),
                  ),
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        color: Colors.amberAccent, shape: BoxShape.circle),
                  ),
                  Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        color: Colors.deepPurpleAccent, shape: BoxShape.circle),
                  ),
                ],
              )
            ]),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
