import 'dart:async';

void main() {
  print("Ready. Sing");
  print("");

  var timer = Timer(
      Duration(seconds: 1), () => print("One more moment of this silence"));
  var timer1 =
      Timer(Duration(seconds: 2), () => print('The loneliness is haunting me'));
  var timer2 = Timer(Duration(seconds: 3),
      () => print("And the weight of the worlds getting harder to hold up"));

  var timer4 = Timer(Duration(seconds: 4), () => print(""));

  var timer5 = Timer(
      Duration(seconds: 5), () => print("It comes in waves, I close my eyes"));
  var timer6 = Timer(
      Duration(seconds: 6), () => print('Hold my breath and let it bury me'));
  var timer7 = Timer(
      Duration(seconds: 7), () => print("I'm not OK and it's not alright"));
  var timer8 = Timer(Duration(seconds: 8),
      () => print("Won't you drag the lake and bring me home again"));

  Future.delayed(Duration(seconds: 9), () => print(''));
  Future.delayed(Duration(seconds: 10), () => print('Who will fix me now?'));
  Future.delayed(Duration(seconds: 11), () => print("Dive in when I'm down?"));
  Future.delayed(Duration(seconds: 12), () => print('Save me from myself'));
  Future.delayed(Duration(seconds: 13), () => print("Don't let me drown"));
}
